""" Modules to receive JSON data and store them as
    custom SQLite model NewsItem
"""
import requests
from flaskblog.models import NewsItem
from datetime import datetime
from flaskblog import db, app

URL = 'https://hacker-news.firebaseio.com/v0/topstories.json?'

top_stories = requests.get(URL)
top_stories_json = top_stories.json()[:30]


if __name__ == "__main__":
    app.app_context().push()
    db.create_all()
    for item_id in top_stories_json:
        news_item = requests.get(f'https://hacker-news.firebaseio.com/v0/item/{item_id}.json?')
        news_item_json = news_item.json()
        if news_item_json.get('type') == 'story':
            existing_item = db.session.query(NewsItem).filter_by(id=item_id).first()
            if not existing_item:
                news = NewsItem(
                    id=item_id,
                    by=news_item_json.get('by'),
                    descendants=news_item_json.get('descendants'),
                    score=news_item_json.get('score'),
                    time=datetime.fromtimestamp(news_item_json.get('time')),
                    title=news_item_json.get('title'),
                    type=news_item_json.get('type'),
                   url=news_item_json.get('url')
                )
            db.session.add(news)
            db.session.commit()
