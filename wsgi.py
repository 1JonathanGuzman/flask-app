""" Module holding the variable app representing the
    flaskblog SQLAlchemy environment
"""
from flaskblog import app

if __name__ == "__main__":
    app.run()
