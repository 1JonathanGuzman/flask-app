""" All models created in models.py imported.
    app environment and db created in __init__.py
    imported. Additionally, authlib imported to
    provide user authentication, service of choice is
    Auth0. Additional tools for handling HTML request
    and json data such as jsonify, request, render_template
    also imported.
"""
import json
import sys
from os import environ as env
from urllib.parse import quote_plus, urlencode

from authlib.integrations.flask_client import OAuth
from dotenv import find_dotenv, load_dotenv
from flask import ( redirect, render_template, session,
                    url_for, jsonify, request, flash )
from flaskblog import app, db
from flaskblog.models import User, Post, NewsItem, Vote
from flaskblog.forms import PostForm
from datetime import datetime

import logging



logging.basicConfig(filename="token_keys.log", level=logging.DEBUG)

AUTH0_CLIENT_ID="NNk2oLdN96WqFBf5eFieekQPCfaJXGIX"
AUTH0_CLIENT_SECRET="qIFp7dKvoEyTTiiltm6JdVZ0SBJqnrLT9NH_tyzNh-xNTGxD2GlHgZRNccE3gzJD"
AUTH0_DOMAIN="dev-uby86k0xeo0xhnel.us.auth0.com"
APP_SECRET_KEY="305af1016d05b88d8435d7fa6c20535a00a3c616726c95bb82dfc97064704c04"
AUTH0_CALLBACK_URL="http://my.xyzcheap.online/callback"

ENV_FILE = find_dotenv()
if ENV_FILE:
    print(load_dotenv(ENV_FILE))


app.secret_key = APP_SECRET_KEY

oauth = OAuth(app)

oauth.register(
    "auth0",
    client_id=AUTH0_CLIENT_ID,
    client_secret=AUTH0_CLIENT_SECRET,
    client_kwargs={
        "scope": "openid profile email",
    },
    server_metadata_url=
                 f'https://{AUTH0_DOMAIN}/.well-known/openid-configuration'
)


@app.route("/login")
def login():
    """ Route called when the user accesses the login
        page for Auth0
    """
    return oauth.auth0.authorize_redirect(
        redirect_uri=url_for("callback", _external=True)
    )

@app.route("/callback", methods=["GET", "POST"])
def callback():
    """ Route called when the user successfully authenticates
        at the Auth0 login page. Handles scenarios such as
        a new user logging in vs an old user returning
    """
    token = oauth.auth0.authorize_access_token()
    session["admin"] = False

    session["userinfo"] = token["userinfo"]
    user = session["userinfo"]

    existing_item = db.session.query(User).filter_by(email=user.email).first()
    # If the user doesn't exist in the database, add them to it
    if not existing_item and user:
        newUser = User(
            username=user.name,
            email=user.email,
            image_file=user.picture,
            posts=[]
        )
        db.session.add(newUser)
        session["user"] = newUser.to_dict()
    else:
        session["user"] = existing_item.to_dict()
    db.session.commit()
    return redirect("/")

@app.route("/logout")
def logout():
    """ Route called when the user presses the logout button.
        Clears the session so that the user information stored is
        no longer there.
    """
    session.clear()
    return redirect(
        "https://" + AUTH0_DOMAIN
        + "/v2/logout?"
        + urlencode(
            {
                "returnTo": url_for("hello", _external=True),
                "client_id": AUTH0_CLIENT_ID,
            },
            quote_via=quote_plus,
        )
    )


@app.route("/")
@app.route("/home")
def hello():
    """ Represents the home page of the website."""
    posts = Post.query.all()
    if "userinfo" in session:
        user = session["userinfo"]
    else:
        user = None
    return render_template('home.html', posts=posts, user=user)

@app.route("/about")
def about():
    """ Represents the about page of the website.
        Currently this page is blank and does not
        display anything.
    """
    if "user" in session:
        user = session["user"]
    else:
        user = None

    if "admin" in session:
        admin = session["admin"]
    else:
        admin = None


    return render_template('about.html', title='About',
                           user=user, admin=admin)

@app.route("/account", methods=["GET", "POST"])
def account():
    """ Route called when a registered user accesses
        the login page where they can view their profile
        details.
    """
    if "user" in session:
        user = session["user"]
    else:
        user = None
    if "admin" in session:
        admin = session["admin"]
    else:
        admin = None

    logging.debug(f"Attributes of user: {print(user['username'] + ' ' + user['email'])}")
    return render_template('account.html', title='Account',
                           image_file=user['image_file'],
                           username=user['username'],
                           email=user['email'],
                           user=user,
                           admin=admin)

@app.route("/admin")
def admin():
    """ Route called when a registered user turns on admin
        admin privileges by pressing the Turn on/ff Admin button
        which grants or revokes admin privileges to the user.
    """
    user = session["user"]
    session["admin"] = not session["admin"]
    return render_template('account.html', title='Account',
                           image_file=user['image_file'],
                           username=user['username'],
                           email=user['email'],
                           user=user,
                           admin=session["admin"])

@app.route("/post/new", methods=["GET", "POST"])
def new_post():
    """ Route called when the user attempts to submit a new post """
    user = session["user"]
    currUser = db.session.query(User).filter_by(email=user['email']).first()
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data,
                    content=form.content.data, author=currUser)
        db.session.add(post)
        db.session.commit()
        flash('Your post has been created!', 'success')
        return redirect(url_for('hello'))
    return render_template('create_post.html', title='New Post',
                           form=form, user=user)

@app.route("/newsfeed")
def newsfeed():
    """ API to return the latest stored news items in a json
        format. All items returned here are pulled from the
        SQLite database.
    """
    payload = []
    news_items = NewsItem.query.all()
    count = 0
    for item in news_items:
        currDict = {}
        currDict['id'] = item.id
        currDict['by'] = item.by
        currDict['descendants'] = item.descendants
        currDict['score'] = item.score
        currDict['time'] = item.time
        currDict['title'] = item.title
        currDict['type'] = item.type
        currDict['url'] = item.url
        payload.append(currDict)
        count += 1
    return jsonify(payload)

@app.route("/news")
def news():
    """ News feed page to display the NewsItem
        objects stored in the database in a
        manner suitable for end-users of the site
    """
    if "user" in session:
        user = session["user"]
    else:
        user = None

    if "admin" in session:
        admin = session["admin"]
    else:
        admin = None
    page = request.args.get('page', 1, type=int)
    news_items = (
        NewsItem.query.order_by(NewsItem.time.desc())
        .paginate(per_page=5, page=page)
    )
    return render_template('news.html', news_items=news_items,
                           user=user, admin=admin)

@app.route("/vote")
def vote():
    """ Route that is called when a registered user places a like,
        dislike, or when an admin tries to delete a NewsItem. All
        logic for these operations are contained here, and handle
        situations such as when unauthorized users make requests
        they are not allowed to make.
    """
    user = session["user"]
    currUser = db.session.query(User).filter_by(email=user['email']).first()
    page = request.args.get('page', 1, type=int)
    vote = request.args.get('vote', 1, type=int)
    news_item_id = request.args.get('news_item_id', 1, type=int)
    news_item = db.session.query(NewsItem).filter_by(id=news_item_id).first()
    news_items = (
        NewsItem.query.order_by(NewsItem.time.desc())
        .paginate(per_page=5, page=page)
    )
    if not user:
        flash("Only registered users can vote on stories", "error")
        return render_template('news.html', news_items=news_items,
                               user=user, admin=session["admin"])

    if not news_item:
        return render_template('news.html', news_items=news_items,
                               user=user, admin=session["admin"])

    if vote == 1:
        like = True
    elif vote == 0:
        like = False
    elif vote == 2:
        admin = session["admin"]
        if admin:
            db.session.delete(news_item)
            db.session.commit()
            flash("NewsItem deleted", "info")
            news_items = (
                NewsItem.query.order_by(NewsItem.time.desc())
                .paginate(per_page=5, page=page)
            )
            return render_template('news.html', news_items=news_items,
                                   user=user, admin=session["admin"])
        elif admin == None:
             flash("Admin is not defined", "error")
        elif admin == False:
             flash("User is not an admin", "warning")
    else:
        flash("Invalid request.", "error")
        return render_template('news.html', news_items=news_items,
                               user=user, admin=session["admin"])

    existing_vote = (
        db.session.query(Vote)
        .filter_by(user_id=currUser.id, news_id=news_item_id)
        .first()
    )
    if not existing_vote:
        # If user hasn't voted on this item
        vote = Vote(like=like, user_id=currUser.id,
                   news_id=news_item_id)
        db.session.add(vote)
        db.session.commit()
        flash("Placed your vote", "success")
    elif existing_vote and existing_vote.like == like:
        # If user has voted this way already on this item
        flash("You've already placed the same vote on this item", "warning")
    elif existing_vote and existing_vote.like != like:
        # If user has voted a different way on this same item
        existing_vote.like = like
        db.session.commit()
        flash("Changed your vote", "success")

    return render_template('news.html', news_items=news_items,
                           user=user, admin=session["admin"])
