""" Importing db representing the SQLite database
    so the models can be connected to and used as
    objects in the database.Also importing datetime
    for use in the fields of several models.
"""
from flaskblog import db
from datetime import datetime



class User(db.Model):
    """ A model used to represent a unique user in the website """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default="default.jpg")
    posts = db.relationship("Post", backref="author",
                            cascade="all, delete-orphan",
                            passive_deletes=True, lazy=True)
    votes = db.relationship("Vote", backref="author",
                            cascade="all, delete-orphan",
                            passive_deletes=True, lazy=True)

    def __repr__(self):
        """ A function to define the string representation of how
            an User should be returned in a query.
        """
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"

    def to_dict(self):
        """ A function to define the dictionary representation of how
            a User should be defined, making it JSON serializable.
        """
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'image_file': self.image_file,
            'posts': [post.to_dict() for post in self.posts],
        }

class Post(db.Model):
    """ A model used to represent a unique post made by a user
        in the website
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False,
                            default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('user.id', name='fk_post_user_id'),
                        nullable=False)

    def __repr__(self):
        """ A function to define the string representation of how
            an Post should be returned in a query
        """
        return f"Post('{self.title}', '{self.date_posted}')"

    def to_dict(self):
        """ A function to define the dictionary representation of how
            a Post should be defined, making it JSON serializable
        """
        return {
            'id': self.id,
            'title': self.title,
            'date_posted': self.date_posted.strftime("%Y-%m-%d %H:%M:%S"),
            'content': self.content,
            'user_id': self.user_id,
        }

class NewsItem(db.Model):
    """ A model used to represent a unique news story in the website """
    id = db.Column(db.Integer, primary_key=True)
    by = db.Column(db.String(100))
    descendants = db.Column(db.Integer)
    score = db.Column(db.Integer)
    time = db.Column(db.DateTime)
    title = db.Column(db.String(250))
    type = db.Column(db.String(10), nullable=False)
    url = db.Column(db.String(200))
    votes = db.relationship('Vote', cascade="all, delete-orphan",
                            backref="news_item", passive_deletes=True)

    def __repr__(self):
        """ A function to define the string representation of how
            an NewsItem should be returned in a query
        """
        return f"NewsItem('{self.title}', '{self.by}', '{self.time}')"

    def to_dict(self):
        """ A function to define the dictionary representation of how
            a NewsItem should be defined, making it JSON serializable
        """
        return {
            'id': self.id,
            'by': self.by,
            'descendants': self.descendants,
            'score': self.score,
            'time': self.time,
            'title': self.title,
            'type': self.type,
            'url': self.url,
        }


    def like_count(self):
        """ A function to provide a way to dynamically run a query
            to retrieve all associated votes where like=True such that
            the query can be run when the render_template function is called
        """
        return Vote.query.filter_by(news_item=self, like=True).count()

    def dislike_count(self):
        """ A function to provide a way to dynamically run a query
            to retrieve all associated votes where like=False such that
            the query can be run when the render_template function is called
        """
        return Vote.query.filter_by(news_item=self, like=False).count()

class Vote(db.Model):
    """ A model used to represent a unique like or dislike
        placed on a specific news story by a specific user
        in the website
    """

    id = db.Column(db.Integer, primary_key=True)
    like = db.Column(db.Boolean, nullable=False)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('user.id', name='fk_vote_user_id'),
                        nullable=False)
    news_id = db.Column(db.Integer,
                        db.ForeignKey('news_item.id', name='fk_vote_news_id'),
                        nullable=False)

    def to_dict(self):
        """ A function to define the dictionary representation of how
            a Vote should be defined, making it JSON serializable
        """
        return {
            'id': self.id,
            'like': self.like,
            'user_id': self.user_id,
            'news_id': self.news_id,
        }
