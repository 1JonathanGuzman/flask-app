""" Modules that form the backbone of the project
    Flask is the framework that powers the flaskblog
    project. SQLAlchemy powers interaction between
    the app and the SQLite database. Migrate was used
    during development to implement any changes in
    schema after records of a certain model were already
    inserted in to the SQLite database
"""
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///site.db"
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from flaskblog import routes
