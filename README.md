Instructions to run the application:

1. Clone the repository using "git clone https://gitlab.com/1JonathanGuzman/flask-app"

2. Use the text editor of your choice to open the flask-app/flaskblog/__init__.py file for writing

3. Replace line 13 of flask-app/flaskblog/__init__.py with:
        app = Flask(__name__, static_folder="{SFolder}")
where {SFolder} is replaced with a string literal representing the absolute file path for the flask-app/static folder on your       device. For example, on a device where the user, "vboxuser", cloned the repository on their ~/ directory, they might modify the line to read: app = Flask(__name__, static_folder="/home/vboxuser/flask-app/static")


4. In the flask-app/flaskblog directory, run the command "python3 -m pip install -r requirements.txt 

5. In the flask-app/ directory
    For the development environment:
        run the command: "python3 run.py"
    For the production environment:
        run the command: "python3 wsgi.py"

6. Open a browser on the local machine and visit the url http://localhost:5000

7. Logging and debugging information related to the web app execution will show up in a file named token_keys.log
